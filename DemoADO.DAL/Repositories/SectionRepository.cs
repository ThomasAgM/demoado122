﻿using DemoADO.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoADO.DAL.Repositories
{
    public class SectionRepository
    {
        private string _connectionString;
        private string _providerName;

        public SectionRepository(string connectionString, string providerName)
        {
            _connectionString = connectionString;
            _providerName = providerName;
        }

        public List<Section> Get(int limit = 20, int offset = 0)
        {
            List<Section> liste = new List<Section>();

            using (IDbConnection conn = GetConnection())
            {
                conn.Open();

                IDbCommand cmd = CreateCommand(
                    conn,
                    "SELECT * FROM Section",null
                    );

                IDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Section s = new Section();
                    s.Id = (int)reader["id"];
                    s.Name = reader["Name"] as string;
                    liste.Add(s);
                }
                return liste;
            }
        }

        public Section GetById(int id)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();

                IDbCommand cmd = CreateCommand(
                    conn,
                    "SELECT * FROM Section WHERE id = @id",
                    new Dictionary<string, object> { { "@id",id } }
                    );

                IDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return new Section 
                    { 
                        Id = (int)reader["id"],
                        Name = reader["Name"] as string
                    };
                }
                return null;
            }

        }

        public int Insert(Section s)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
            }
            throw new NotImplementedException();
        }

        public bool Update(Section s)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
            }
            throw new NotImplementedException();
        }

        public bool Delete(int Id)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
            }
            throw new NotImplementedException();
        }

        private IDbConnection GetConnection()
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(_providerName);

            IDbConnection connection = factory.CreateConnection();

            connection.ConnectionString = _connectionString;

            return connection;
        }

        private IDbCommand CreateCommand(IDbConnection conn, string text, Dictionary<string, object> parameters = null)
        {
            IDbCommand cmd = conn.CreateCommand();
            cmd.CommandText = text;
            if (parameters != null)
            {
                foreach (KeyValuePair<string, object> kvp in parameters)
                {
                    IDataParameter p = cmd.CreateParameter();
                    p.ParameterName = kvp.Key;
                    p.Value = kvp.Value;
                    cmd.Parameters.Add(p);
                }
            }
            return cmd;
        }
    }
}
